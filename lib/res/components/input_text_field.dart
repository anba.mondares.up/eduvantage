

import 'package:flutter/material.dart';
import 'package:tech_media/res/color.dart';

class InputTextField extends StatelessWidget {
  const InputTextField({
    Key? key,
    required this.myController,
    required this.focusNode,
    required this.onFiledSubmitValue,
    required this.keyBoardType,
    required this.obscureText,
    required this.hint,
    this.enable = true,
    required this.onValidator,
    this.autoFocus = false

  }) : super(key: key);

  final TextEditingController myController;
  final FocusNode focusNode;
  final FormFieldSetter onFiledSubmitValue;
  final FormFieldValidator onValidator;

  final TextInputType keyBoardType;
  final String hint;
  final bool obscureText;
  final bool enable, autoFocus;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 0),
      child: TextFormField(
        controller: myController,
        focusNode: focusNode,
        obscureText: obscureText,
        onFieldSubmitted: onFiledSubmitValue,
        validator: onValidator,
        keyboardType: keyBoardType,
        cursorColor: AppColors.primaryColor,
        showCursor: false,
        cursorHeight: 18,
        style: Theme.of(context).textTheme.bodyText2!.copyWith(height: 0,fontSize: 18),
        decoration: InputDecoration(
          hintText: hint,
          enabled: enable,
          contentPadding: const EdgeInsets.all(20),
          hintStyle: Theme.of(context).textTheme.headline5!.copyWith(height: 0,color: Colors.black.withOpacity(0.6)),
          border: const OutlineInputBorder(
            borderSide: BorderSide(color: AppColors.textFieldDefaultFocus),
            borderRadius: BorderRadius.all(Radius.circular(20))
          ),
          focusedBorder: const OutlineInputBorder(
              borderSide: BorderSide(color: AppColors.secondaryColor),
              borderRadius: BorderRadius.all(Radius.circular(20))
          ),
          errorBorder: const OutlineInputBorder(
              borderSide: BorderSide(color: AppColors.alertColor),
              borderRadius: BorderRadius.all(Radius.circular(20))
          ),
          enabledBorder: const OutlineInputBorder(
              borderSide: BorderSide(color: AppColors.bgColor),
              borderRadius: BorderRadius.all(Radius.circular(20))
          ),

        ),
      ),
    );
  }
}
