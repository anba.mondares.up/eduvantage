
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';
import 'package:tech_media/res/components/input_text_field.dart';
import 'package:tech_media/res/components/round_button.dart';
import 'package:tech_media/view_model/login/login_controller.dart';

import '../../res/color.dart';
import '../../utils/routes/route_name.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {

  final _formKey = GlobalKey<FormState>();

  final emailController = TextEditingController();
  final passwordController = TextEditingController();

  final emailFocusNode = FocusNode();
  final passwordFocusNode = FocusNode();

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();

    emailController.dispose();
    passwordController.dispose();
    emailFocusNode.dispose();
    passwordFocusNode.dispose();

  }

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height * 1;
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
          child: SingleChildScrollView(

            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const SizedBox(height: 50,),
                SizedBox(height: height * .01,),
                Text('Login', style: Theme.of(context).textTheme.displayLarge,),
                SizedBox(height: height * .01,),
                Text("Welcome back!\n Log into your account and get started",
                  textAlign: TextAlign.left,
                  style: Theme.of(context).textTheme.bodyText2,) ,

                SizedBox(height: height * .01,),
                Form(
                    key: _formKey,
                    child: Padding(
                      padding: EdgeInsets.only(top: height * .06, bottom: height * 0.01),
                      child: Column(
                        children: [
                          InputTextField(
                              myController: emailController,
                              focusNode: emailFocusNode,
                              onFiledSubmitValue: (value){

                              },
                              keyBoardType: TextInputType.emailAddress,
                              obscureText: false,
                              hint: 'Email',
                              onValidator: (value){
                                return value.isEmpty ? 'Enter email' : null;
                              }
                          ),

                          SizedBox(height: height * 0.01,),

                          InputTextField(
                              myController: passwordController,
                              focusNode: passwordFocusNode,
                              onFiledSubmitValue: (value){

                              },
                              keyBoardType: TextInputType.emailAddress,
                              obscureText: true,
                              hint: 'Password',
                              onValidator: (value){
                                return value.isEmpty ? 'Enter password' : null;
                              }
                          ),

                        ],
                      ),
                    )
                ),


                Align(
                  alignment: Alignment.centerRight,
                  child: GestureDetector(
                    onTap: (){
                  Navigator.pushNamed(context, RouteName.forgotScreen);
                    },
                    child: Text(
                        'Forgot Password?',
                            style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                                fontSize: 15,
                                decoration: TextDecoration.underline
                            )
                        ),
                  ),
                ),


                const SizedBox(height: 40,),
                ChangeNotifierProvider(
                  create: (_) => LoginController(),
                  child: Consumer<LoginController>(
                      builder: (context, provider, child){
                        return RoundButton(
                            title: 'Login',
                            loading: provider.loading,
                            onPress: (){
                              if(_formKey.currentState!.validate()){
                                provider.login(context, emailController.text, passwordController.text);

                              }

                            }
                        );
                      },
                  ),
                ),
                SizedBox(height: height * .03,),

                InkWell(
                  onTap: (){
                    Navigator.pushNamed(context, RouteName.signUpScreen);
                  },
                  child: Text.rich(
                    TextSpan(
                      text: "Don't have an account?",
                      style: Theme.of(context).textTheme.headline6!.copyWith(fontSize: 15),
                      children: [
                        TextSpan(
                          text: 'Sign Up',
                          style: Theme.of(context).textTheme.headlineMedium!.copyWith(
                              fontSize: 15,
                          decoration: TextDecoration.underline
                          )
                        )
                      ]
                    )
                  ),
                )

              ],
            ),
          ),
        ),
      ),
    );
  }
}
