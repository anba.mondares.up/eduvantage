import 'package:provider/provider.dart';
import 'package:tech_media/res/components/input_text_field.dart';
import 'package:tech_media/res/components/round_button.dart';
import 'package:flutter/material.dart';
import 'package:tech_media/utils/routes/route_name.dart';
import 'package:tech_media/utils/utils.dart';
import 'package:tech_media/view_model/signup/signup_controller.dart';

import '../../res/color.dart';

class SignUpScreen extends StatefulWidget {
  const SignUpScreen({Key? key}) : super(key: key);

  @override
  State<SignUpScreen> createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {

  final _formKey = GlobalKey<FormState>();

  final emailController = TextEditingController();
  final passwordController = TextEditingController();

  final userNameController = TextEditingController();

  final emailFocusNode = FocusNode();
  final userNameFocusNode = FocusNode();
  final passwordFocusNode = FocusNode();

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();

    emailController.dispose();
    passwordController.dispose();
    emailFocusNode.dispose();
    passwordFocusNode.dispose();

  }

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height * 1;
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        leading: const BackButton(
          color: AppColors.bgColor,
        ),
        elevation: 0,
        backgroundColor: Colors.white,
      ),
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
          child: ChangeNotifierProvider(
            create: (_) => SignUpController(),
            child: Consumer<SignUpController>(
              builder: (context, provider, child){
                 return SingleChildScrollView(

                   child: Column(
                     mainAxisAlignment: MainAxisAlignment.start,
                     crossAxisAlignment: CrossAxisAlignment.center,
                     children: [
                       SizedBox(height: height * .01,),
                       Text('Sign Up', style: Theme.of(context).textTheme.displayMedium,),
                       SizedBox(height: height * .01,),
                       Text('Start learning\nSign Up Today!',
                         textAlign: TextAlign.center,
                         style: Theme.of(context).textTheme.bodyText2,),
                       SizedBox(height: height * .01,),
                       Form(
                           key: _formKey,
                           child: Padding(
                             padding: EdgeInsets.only(top: height * .06, bottom: height * 0.01),
                             child: Column(
                               children: [

                                 InputTextField(
                                     myController: userNameController,
                                     focusNode: userNameFocusNode,
                                     onFiledSubmitValue: (value){

                                     },
                                     keyBoardType: TextInputType.emailAddress,
                                     obscureText: false,
                                     hint: 'Username',
                                     onValidator: (value){
                                       return value.isEmpty ? 'Enter username' : null;
                                     }
                                 ),
                                 SizedBox(height: height * 0.01,),

                                 InputTextField(
                                     myController: emailController,
                                     focusNode: emailFocusNode,
                                     onFiledSubmitValue: (value){
                                       Utils.fieldFocus(context, emailFocusNode, passwordFocusNode);

                                     },
                                     keyBoardType: TextInputType.emailAddress,
                                     obscureText: false,
                                     hint: 'Email',
                                     onValidator: (value){
                                       return value.isEmpty ? 'Enter email' : null;
                                     }
                                 ),

                                 SizedBox(height: height * 0.01,),

                                 InputTextField(
                                     myController: passwordController,
                                     focusNode: passwordFocusNode,
                                     onFiledSubmitValue: (value){

                                     },
                                     keyBoardType: TextInputType.emailAddress,
                                     obscureText: true,
                                     hint: 'Password',
                                     onValidator: (value){
                                       return value.isEmpty ? 'Enter password' : null;
                                     }
                                 ),

                               ],
                             ),
                           )
                       ),



                       const SizedBox(height: 40,),
                       RoundButton(
                           title: 'Sign Up',
                           loading: provider.loading,
                           onPress: () {
                             print('tap');
                             if (_formKey.currentState!.validate()) {
                             provider.signup(context, userNameController.text, emailController.text, passwordController.text);
                             }
                           }
                       ),
                       SizedBox(height: height * .03,),


                     ],
                   ),
                 );
              },
            ),
          ),
        ),
      ),
    );
  }
}
