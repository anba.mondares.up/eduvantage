import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:persistent_bottom_nav_bar/persistent_tab_view.dart';
import 'package:tech_media/res/color.dart';
import 'package:tech_media/utils/routes/route_name.dart';
import 'package:tech_media/view/dashboard/chat/chat.dart';
import 'package:tech_media/view/dashboard/profile/profile.dart';
import 'package:tech_media/view/dashboard/user/user_list_screen.dart';
import 'package:tech_media/view_model/services/session_manager.dart';

import 'home/home.dart';
import 'notifications/notifications.dart';

class DashboardScreen extends StatefulWidget {
  const DashboardScreen({Key? key}) : super(key: key);

  @override
  State<DashboardScreen> createState() => _DashboardScreenState();
}

class _DashboardScreenState extends State<DashboardScreen> {


  final controller = PersistentTabController(initialIndex: 0);
  List <Widget>_buildScreen(){

    return [
      HomeScreen(),
      UserListScreen(),
      NotificationScreen(),
      ProfileScreen(),
    ];
  }

  List<PersistentBottomNavBarItem> _navBarItem(){
    return[
      PersistentBottomNavBarItem(
          icon: Icon(Icons.home_rounded, color: AppColors.whiteColor),
        activeColorPrimary: AppColors.secondaryColor,
        inactiveIcon: Icon(Icons.home_rounded , color: Colors.grey.shade100,)
      ),

      PersistentBottomNavBarItem(
          icon: Icon(Icons.task,color: AppColors.whiteColor),
          activeColorPrimary: AppColors.secondaryColor,
    inactiveIcon: Icon(Icons.message_rounded , color: Colors.grey.shade100,)
    ),

      PersistentBottomNavBarItem(
          icon: Icon(Icons.book,color: AppColors.whiteColor),
          activeColorPrimary: AppColors.secondaryColor,
    inactiveIcon: Icon(Icons.notifications_rounded , color: Colors.grey.shade100,)
    ),

      PersistentBottomNavBarItem(
          icon: Icon(Icons.person_rounded,color: AppColors.whiteColor),
          activeColorPrimary: AppColors.secondaryColor,
    inactiveIcon: Icon(Icons.person_rounded , color: Colors.grey.shade100,)
    ),
    ];
  }

  @override
  Widget build(BuildContext context) {

    return PersistentTabView(
        context,
        screens: _buildScreen(),
          items: _navBarItem(),
      controller: controller,
      backgroundColor: AppColors.bgColor,
      decoration: NavBarDecoration(
        colorBehindNavBar: AppColors.bgColor,
        borderRadius: BorderRadius.circular(20)
    ),
      navBarStyle: NavBarStyle.style10,



    );
  }
}
