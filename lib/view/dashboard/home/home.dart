import 'package:flutter/material.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:flutter/cupertino.dart';

import '../../../res/color.dart';


class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {

  final ref = FirebaseDatabase.instance.ref('User');

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar:
      PreferredSize(child: getAppBar(), preferredSize: Size.fromHeight(60)),
      body: getBody(),
    );
  }

  Widget getAppBar() {
    return AppBar(
      elevation: 0,
      backgroundColor: Colors.white,
      title: Padding(
        padding: const EdgeInsets.only(left: 5),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              "EduVantage",
              style: TextStyle(
                  fontSize: 30, color: Colors.black, fontWeight: FontWeight.bold),
            ),

            IconButton(
                onPressed: () {},
                icon: Icon(
                  FeatherIcons.user,
                  color: Colors.black,
                  size: 25,
                ))
          ],
        ),
      ),
    );
  }

  Widget getBody() {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.only(left: 18, right: 18),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: 20,
            ),



            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Feed",
                  style: TextStyle(fontSize: 23, fontWeight: FontWeight.bold, color: Colors.black),
                ),
                SizedBox(
                  height: 30,
                ),
                // story profile

                SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    children: [
                      Container(
                        width: 58,
                        height: 58,
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            gradient: LinearGradient(colors: [
                              Colors.black,
                              Colors.black
                            ])),
                        child: Center(
                          child: Icon(
                            Icons.add,
                            color: Colors.white,
                            size: 28,
                          ),
                        ),
                      ),

                      SizedBox(
                        width: 10,
                      ),

                      Container(
                        width: 58,
                        height: 58,
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            gradient: LinearGradient(colors: [
                              Colors.black,
                              Colors.black
                            ])),
                        child: Center(
                          child: Icon(
                            Icons.person_rounded,
                            color: Colors.white,
                            size: 28,
                          ),
                        ),
                      ),

                      SizedBox(
                        width: 10,
                      ),

                      Container(
                        width: 58,
                        height: 58,
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            gradient: LinearGradient(colors: [
                              Colors.black,
                              Colors.black

                            ])),
                        child: Center(
                          child: Icon(
                            Icons.person_rounded,
                            color: Colors.white,
                            size: 28,
                          ),
                        ),
                      ),

                      SizedBox(
                        width: 10,
                      ),

                      Container(
                        width: 58,
                        height: 58,
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            gradient: LinearGradient(colors: [
                              Colors.black,
                              Colors.black

                            ])),
                        child: Center(
                          child: Icon(
                            Icons.person_rounded,
                            color: Colors.white,
                            size: 28,
                          ),
                        ),
                      ),

                      SizedBox(
                        width: 10,
                      ),

                      Container(
                        width: 58,
                        height: 58,
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            gradient: LinearGradient(colors: [
                              Colors.black,
                              Colors.black

                            ])),
                        child: Center(
                          child: Icon(
                            Icons.person_rounded,
                            color: Colors.white,
                            size: 28,
                          ),
                        ),
                      ),

                      SizedBox(
                        width: 10,
                      ),

                      Container(
                        width: 58,
                        height: 58,
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            gradient: LinearGradient(colors: [
                              Colors.black,
                              Colors.black

                            ])),
                        child: Center(
                          child: Icon(
                            Icons.person_rounded,
                            color: Colors.white,
                            size: 28,
                          ),
                        ),
                      ),


                      SizedBox(
                        width: 30,
                      ),

                    ],
                  ),
                )
              ],
            ),
            SizedBox(
              height: 50,
            ),

            Center(
              child: Container(
                width: 320,
                height: 150,
                decoration: BoxDecoration(
                    shape: BoxShape.rectangle,
                    borderRadius: BorderRadius.circular(20),
                    gradient: LinearGradient(colors: [
                       Colors.black,
                       Colors.black

                    ])),
                child: Center(
                  child: Icon(
                    Icons.newspaper_rounded,
                    color: Colors.white,
                    size: 100,
                  ),
                ),
              ),
            ),

            SizedBox(
              height: 20,
            ),



            Center(
              child: Container(
                width: 320,
                height: 280,
                decoration: BoxDecoration(
                    shape: BoxShape.rectangle,
                    borderRadius: BorderRadius.circular(20),
                    gradient: LinearGradient(colors: [
                      Colors.black,
                      Colors.black

                    ])),
                child: Center(
                  child: Icon(
                    Icons.image_outlined,
                    color: Colors.white,
                    size: 200,
                  ),
                ),
              ),
            ),

            SizedBox(
              height: 20,
            ),



            Center(
              child: Container(
                width: 320,
                height: 280,
                decoration: BoxDecoration(
                    shape: BoxShape.rectangle,
                    borderRadius: BorderRadius.circular(20),
                    gradient: LinearGradient(colors: [
                      Colors.black,
                      Colors.black

                    ])),
                child: Center(
                  child: Icon(
                    Icons.image_outlined,
                    color: Colors.white,
                    size: 200,
                  ),
                ),
              ),
            ),






            SizedBox(
              height: 5,
            ),







          ],
        ),
      ),
  );
  }
}

