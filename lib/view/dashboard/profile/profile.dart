
import 'dart:io';

import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tech_media/res/color.dart';
import 'package:tech_media/res/components/round_button.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:tech_media/view/login/login_screen.dart';
import 'package:tech_media/view/splash/splash_screen.dart';
import 'package:tech_media/view_model/profile/profile_controller.dart';
import 'package:tech_media/view_model/services/session_manager.dart';

class ProfileScreen extends StatefulWidget {
  const ProfileScreen({Key? key}) : super(key: key);

  @override
  State<ProfileScreen> createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {

  final ref = FirebaseDatabase.instance.ref('User');
  final auth = FirebaseAuth.instance;


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.white,
      body: ChangeNotifierProvider(
        create: (_) => ProfileController(),
        child: Consumer<ProfileController>(
          builder: (context, provider, child){
            return SafeArea(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: StreamBuilder(
                  stream: ref.child(SessionController().userId.toString()).onValue,
                  builder: (context,AsyncSnapshot snapshot) {
                    if (!snapshot.hasData){
                      return Center(child: CircularProgressIndicator());
                    }else if (snapshot.hasData) {
                      Map<dynamic, dynamic> map = snapshot.data.snapshot.value;

                      return Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          const SizedBox(height: 20,),
                          Stack(
                            alignment: Alignment.bottomCenter,
                            children: [
                              Padding(
                                padding: const EdgeInsets.symmetric(vertical: 10),
                                child: Center(
                                  child: Container(
                                    height: 130,
                                    width: 130,
                                    decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        boxShadow: [
                                          BoxShadow(color: AppColors.bgColor)
                                        ],
                                        border: Border.all(
                                          color: AppColors.bgColor,
                                        )
                                    ),
                                    child: ClipRRect(
                                      borderRadius: BorderRadius.circular(100),
                                      child: provider.image == null ? map['profile'].toString()  == "" ? const Icon(Icons.person_rounded, size: 70, color: AppColors.whiteColor,) :
                                      Image(
                                        fit: BoxFit.cover,
                                        image: NetworkImage(map['profile'].toString(),)
                                        ,
                                        loadingBuilder: (context, child, loadingProgress) {
                                          if (loadingProgress == null) return child;

                                          return Center(child: CircularProgressIndicator());
                                        },
                                        errorBuilder: (context, object, stack) {
                                          return Container(
                                            child: Icon(Icons.error_outline,
                                              color: AppColors.alertColor,),
                                          );
                                        },

                                      ) :

                                          Stack(
                                            children: [
                                              Image.file(
                                               File(provider.image!.path).absolute
                                              ),
                                              Center(child: CircularProgressIndicator())
                                            ],
                                          ),



                                    ),
                                  ),
                                ),
                              ),
                              InkWell(
                                onTap: (){
                                  provider.pickImage(context);
                                },
                                child: CircleAvatar(
                                  radius: 14,
                                  backgroundColor: AppColors.bgColor2,
                                  child: Icon(Icons.camera_alt_rounded, size: 18, color: AppColors.whiteColor,),
                                ),
                              )

                            ],

                          ),
                          const SizedBox(height: 0,),
                          Text(map['userName'],
                              textAlign: TextAlign.center,
                              style: Theme.of(context).textTheme.subtitle2) ,
                          const SizedBox(height: 20,),
                          Container(
                            height: 2,
                            color: AppColors.primaryColor,
                            margin: EdgeInsets.symmetric(horizontal: 15),
                          ),

                          const SizedBox(height: 20,),
                          GestureDetector(
                              onTap: (){
                                provider.showUserNameDialogAlert(context, map['userName']);
                              },
                              child: ReusableRow(title: 'Username', value: map['userName'], iconData: Icons.person_outline,)),
                          GestureDetector(
                            onTap: (){
                              provider.showPhoneDialogAlert(context, map['phone']);
                            },
                              child: ReusableRow(title: 'Phone', value: map['phone'] == '' ? 'xxx-xxx-xxx' : map['phone'], iconData: Icons.phone_outlined,)),
                          ReusableRow(title: 'Email', value: map['email'], iconData: Icons.email_outlined,),
                          const SizedBox(height: 20,),
                          RoundButton(title: 'Logout', color: AppColors.bgColor, textColor: Colors.white, onPress: (){
                            auth.signOut();
                            Navigator.canPop(context) ? Navigator.pop(context) : null;
                            Navigator.pushReplacement(context, MaterialPageRoute(builder: (_) => const LoginScreen()));
                          }),

                        ],
                      );

                    }else{
                      return Center(child: Text('Something went wrong', style: Theme.of(context).textTheme.subtitle1,));
                    }

                  },
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}


class ReusableRow extends StatelessWidget {
  final String title, value;
  final IconData iconData;
  const ReusableRow({Key? key, required this.title, required this.iconData, required this. value}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        ListTile(
          title: Text(title, style: Theme.of(context).textTheme.headline3,),
          leading: Icon(iconData, color: AppColors.bgColor2,),
          trailing: Text(value,style: Theme.of(context).textTheme.headline3, ),
        ),
        Divider(color: AppColors.whiteColor.withOpacity(0))
      ],
    );
  }
}








